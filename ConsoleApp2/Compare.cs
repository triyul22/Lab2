﻿using System;
namespace Calculator
{
    public class Compare<T> where T : IComparable<T>
    {
        /// <summary>
        /// Сравнение двух чисел. Возвращает номер большего аргумента. В случае равенства выводит 0.
        /// </summary>
        /// <param name="Number1"></param>
        /// <param name="Number2"></param>
        /// <returns>0, 1 or 2</returns>
        public static int Max(T Number1, T Number2)
        {
            if (Number1.CompareTo(Number2) > 0)
                return 1;
            else if (Number1.CompareTo(Number2) < 0)
                return 2;
            else return 0;
        }
        /// <summary>
        /// Сравнение двух чисел. Возвращает номер меньшего аргумента. В случае равенства выводит 0.
        /// </summary>
        /// <param name="Number1"></param>
        /// <param name="Number2"></param>
        /// <returns>0, 1 or 2</returns>
        public static int Min(T Number1, T Number2)
        {
            if (Number1.CompareTo(Number2) > 0)
                return 2;
            else if (Number1.CompareTo(Number2) < 0)
                return 1;
            else return 0;
        }
        /// <summary>
        /// Сравнение двух чисел. Возвращает значение true в случае равенства. В противном случае False
        /// </summary>
        /// <param name="Number1"></param>
        /// <param name="Number2"></param>
        /// <returns>true/false</returns>
        public static bool IsEqual(T Number1, T Number2)
        {
            return (Number1.CompareTo(Number2) == 0);
        }
    }
}
